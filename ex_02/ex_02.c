#include <stdlib.h>

#include "struct.h"

void	my_init(t_my_struct *s)
{
  if (!s)
    return ;

  s->id = 0;
  s->str = NULL;
}
