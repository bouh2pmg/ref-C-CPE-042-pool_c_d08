#ifndef ABS_H_
# define ABS_H_

# define MY_ABS(nb) ((nb >= 0) ? (nb) : (-nb))

#endif /* !ABS_H_ */
