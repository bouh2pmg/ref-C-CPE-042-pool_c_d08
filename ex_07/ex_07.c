// No Header
// 10/10/2017 - 11:16:45

int	fib_it(int n)
{
  int	tmp;
  int	f1;
  int	f2;

  if (n < 0)
    return (-1);
  f1 = 0;
  f2 = 1;
  while (0 < n)
    {
      tmp = f1;
      f1 = f2;
      f2 = f1 + tmp;
      n--;
    }
  return (f1);
}
