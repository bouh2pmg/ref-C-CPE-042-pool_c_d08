#include <string.h>

#include "struct.h"
#include "abs.h"

void	my_init(t_my_struct *s, int id, const char *text)
{
  if (!s)
    return ;

  s->id = MY_ABS(id);
  s->str = strdup(text);
}
