int	my_power_it(int nb, int power)
{
  int	ret;

  ret = 1;
  while (power-- > 0)
    ret *= nb;
  return ret;
}
