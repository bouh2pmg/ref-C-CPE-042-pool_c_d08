// No Header
// 10/10/2017 - 11:16:20

int	fib(int n, int f1, int f2)
{
  return ((n == 0) ? f1 : (fib(n - 1, f2, f1 + f2)));
}

int	fib_rec(int n)
{
  if (n >= 0)
    return (fib(n, 0, 1));
  return (-1);
}
